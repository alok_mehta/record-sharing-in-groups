trigger CaseTrigger on Case (after insert, after update) {
    if(trigger.isAfter && trigger.isInsert)
    CaseTriggerHnadler.afterInsert(trigger.new);
   
    if(trigger.isAfter && trigger.isUpdate)
     CaseTriggerHnadler.afterUpdate(trigger.new, trigger.oldMap);

}