public  with sharing class CaseTriggerHandler {
    public static void afterInstert(List<Case> newList){
        caseInsertOrUpdate(newList,null);
    }
    public static void afterUpdate(List<Case> newList,Map<Id,Case> oldMap){
        caseInsertOrUpdate(newList,oldMap);
    }
   
    // function to perform both insert and update using single method
    private static void caseInsertOrUpdate(List<Case> newList, Map<Id,Case> oldMap){
        set<Id>highGroupMmberIds = new set<Id>();
        set<Id>lowGroupMmberIds = new set<Id>();
       
        //share cases record set  for update
        set<Id>caseshareId = new set<Id>();
        boolean hasPriority = false;
       
        for(case case1: newList){
            if(case1.Priority == 'High' || case1.Priority == 'Low' ){
                hasPriority = true;
            }
            //for update record
            if(oldMap !=null){
                if(case1.Priority == 'High' || case1.Priority == 'Low' || case1.Priority == 'Medium' ){
                    hasPriority = true;
                    caseshareId.add(case1.Id);
                }
            }
        }
       
        if(hasPriority){
            List<Group> groupId1 = [SELECT Id,Name FROM Group
                                    WHERE Name ='High Case Group'
                                    OR Name ='Low Case Group'];
            system.debug('--groupId1---'+groupId1);
            set<Id> groupIdSet = new set<Id>();
           
            for(Group gropData : groupId1){
                System.debug('--gropData---'+gropData);
                groupIdSet.add(gropData.Id);
               
            }
            List<Group> groupMember = [Select Id,Name,
                                       (select UserOrGroupId  from GroupMembers)
                                       from Group
                                       where Id  in: groupIdSet];
            system.debug('-------groupMember----'+groupMember);
            for(Group gp: groupMember){
                system.debug('-------gp----'+gp + gp.GroupMembers);
                if(gp.Name == 'High Case Group'){
                    for(GroupMember gmIds : gp.GroupMembers){
                        highGroupMmberIds.add(gmIds.UserOrGroupId);
                    }
                }
                else if(gp.Name == 'Low Case Group'){
                    for(GroupMember gmIds : gp.GroupMembers){
                        lowGroupMmberIds.add(gmIds.UserOrGroupId);
                    }
                   
                }
               
            }
        }
       
        //___________for insert_____________ //
        if(oldMap == null){
            List<CaseShare> CaseShareList = new List<CaseShare>();
            for(Case c : newList ){
                if(c.Priority == 'High'){
                    if(highGroupMmberIds.size()>0) {
                        for(Id highId :highGroupMmberIds){
                            if(highId != c.OwnerId){
                                CaseShare cs = new CaseShare();
                                cs.CaseId = c.Id;
                                cs.UserOrGroupId = highId;
                                cs.CaseAccessLevel = 'Read';
                                CaseShareList.add(cs);
                            }
                        }}}
                else if(c.Priority == 'Low'){
                    if(lowGroupMmberIds.size()>0) {
                        for(Id lowId :lowGroupMmberIds){
                            if(lowId != c.OwnerId){
                                CaseShare cs = new CaseShare();
                                cs.CaseId = c.Id;
                                cs.UserOrGroupId = lowId;
                                cs.CaseAccessLevel = 'Read';
                                CaseShareList.add(cs);
                            }
                        }
                    }
                }
            }
           
            if(CaseShareList.size()>0)
                insert CaseShareList;
           
        }
        /* __________for update________*/
        if(oldMap != null){
            system.debug('-----old map---'+oldMap);
            List<CaseShare> CaseShareList = new List<CaseShare>();
           
           
            List<CaseShare> caseShareData = [select id,CaseId,UserOrGroupId
                                             from CaseShare
                                             where CaseId In: caseshareId
                                             AND RowCause = 'Manual'];
           
            for(Case c : newList ){
                if( c.priority != oldMap.get(c.Id).priority){
                    if(c.Priority == 'High'){
                        system.debug('--High-');
                        if(highGroupMmberIds.size()>0) {
                            for(Id highId :highGroupMmberIds){
                                if(highId != c.OwnerId){
                                    CaseShare newCaseShare = new CaseShare();
                                    newCaseShare.CaseId = c.Id;
                                    newCaseShare.UserOrGroupId = highId;
                                    newCaseShare.CaseAccessLevel = 'Read';
                                    CaseShareList.add(newCaseShare);
                                }
                            }
                        }
                    }
                   
                    else if(c.priority != oldMap.get(c.Id).priority){
                        if(c.Priority == 'Low'){
                            system.debug('--low-');
                            if(lowGroupMmberIds.size()>0) {
                                system.debug('--low2-');
                                for(Id lowId :lowGroupMmberIds){
                                    if(lowId != c.OwnerId){
                                        system.debug('--low3-');
                                        CaseShare newCaseShare = new CaseShare();
                                        newCaseShare.CaseId = c.Id;
                                        newCaseShare.UserOrGroupId = lowId;
                                        newCaseShare.CaseAccessLevel = 'Read';
                                        CaseShareList.add(newCaseShare);
                                        system.debug('--low4-');
                                    }
                                }
                            }
                        }
                    }
                }
                else if(c.priority != oldMap.get(c.Id).priority){
                    if(c.Priority == 'Medium'){
                        CaseShare newCaseShare = new CaseShare();
                        newCaseShare.CaseId = c.Id;
                        newCaseShare.UserOrGroupId = c.OwnerId;
                        newCaseShare.CaseAccessLevel = 'Read';
                        CaseShareList.add(newCaseShare);
                    }
                }
                if(caseShareData.size()>0){
                    Database.Delete(caseShareData, false);
                }
                if(CaseShareList.size()>0)
                    insert CaseShareList;
            }
        }
    }
}